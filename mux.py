"""Code for TCA9548A Multiplexer and SBS (Smart Battery System) cards. You can use up to 8 cards."""

from Adafruit_I2C import Adafruit_I2C

class MUX:

        def __init__(self):
                self.MUX_ADDRESS = 0x70 						# I2C ADDRESS of SBS card
                self.SBS_ADDRESS = 0x0b                         # The ADDRESS of I2C MUX
                self.BUSNUM = 1                                 # The number of the I2C bus. Pin 19=SCL , 20=SDA

        def connect(self):
                self.sbs = Adafruit_I2C(self.SBS_ADDRESS,self.BUSNUM)
				self.mux = Adafruit_I2C(self.MUX_ADDRESS,self.BUSNUM)

        def Data(self):                                     # Data from SBS card.You can add any other parameter you want to read .

                temp = self.sbs.readU16(0x08)				# Temperature  0.1K
                voltage = self.sbs.readU16(0x09)				# Voltage   mV
                current = self.sbs.readU16(0x0a)    
                vcell1=self.sbs.readU16(0x3f)				# Cell Voltage 1 , mV
                vcell2=self.sbs.readU16(0x3e)
               	vcell3=self.sbs.readU16(0x3d)
                vcell4=self.sbs.readU16(0x3c)
                rsoc= self.sbs.readU8(0x0d)   				# Relative State of Charge %
                asoc= self.sbs.readU8(0x0e)   				# Absolutely State of Charge  %
                RemainingCapacity= self.sbs.readU16(0x0f)	# Remaining Capacity	mAh
                FullCapacity= self.sbs.readU16(0x10)			# Full Charge Capacity  mAh
                RunTimeToEmpty= self.sbs.readU16(0x11)	  	# minutes
                AverageTimeToEmpty= self.sbs.readU16(0x12)	# minutes
                AverageTimeToFull = self.sbs.readU16(0x13)	# minutes
                ChargingCurrent = self.sbs.readU16(0x14)     # mA
                ChargingVoltage = self.sbs.readU16(0x15)		# mV	
                DesignVoltage = self.sbs.readU16(0x19)		# mV

                tempK= temp/10                                  # Coversion to Celsius
                tempC = tempK -273
        #       print ("\nTemperatura = " + str(temp) +" \n")
        #       print ("\nTemperatura = " + str(tempK) +" K \n")
                print ("\nTemperature = " + str(tempC) +" C")
                print ("\nVoltage = " + str(voltage) + " mV")
                print ("\nCurrent = " + str(current)+" mA")
                print ("\nVCell 1 = " + str(vcell1) + " mV")
                print ("\nVCell 2 = " + str(vcell2) + " mV")
                print ("\nVCell 3 = " + str(vcell3) + " mV")
                print ("\nVCell 4 = " + str(vcell4) + " mV")
                print ("\nRemaining Capacity = " + str(RemainingCapacity)+ " mAh")
                print ("\nFull Capacity = " + str(FullCapacity) + " mAh")
                print ("\nRun Time to Empty = " + str(RunTimeToEmpty) + " minutes")
                print ("\nAverage Time to Empty = " + str(AverageTimeToEmpty)+ " minutes")
                print ("\nAverage Time to Full = " + str(AverageTimeToFull) + " minutes")
                print ("\nCharging current = "+ str(ChargingCurrent)+ " mA")
                print ("\nCharging voltage = " + str(ChargingVoltage)+ " mV")
                print ("\nDesing voltage = " + str(DesignVoltage) + " mV\n")


        def readData(self,channel):

                _channel = 2**(channel-1) 
                self.mux.write8(self.MUX_ADDRESS,_channel)
                self.Data()

        def Reset(self):

                self.mux.write8(self.MUX_ADDRESS,0)